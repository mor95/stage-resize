/**
 * /**
 * @file Archivo que contiene el módulo stage-resize
 * @namespace index
 * @module stage-resize
 */

const $ = require('jquery');
// const presentation = require('presentation');

/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = {
    currentProps: {
        vertical: false,
    },
    /**
     * Función para obtener información de escenario.
     * @constructor
     * @author Some value here
     */
    getStageTransformedData: function(){
        var $stage = $('#stage');
        if(!$stage.length){
            throw new Error('Couldn\'t find #stage element. There needs to be at least one in the DOM.');
            return;
        }

        const windowDimensions = {
            width: window.innerWidth,
            height: window.innerHeight
        }

        const stageDimensions = {
            width: $stage.width(),
            height: $stage.height()
        }

        const aspectRatios = {
            window: windowDimensions.width / windowDimensions.height,
            stage: stageDimensions.width / stageDimensions.height
        }

        var scale;

        if(aspectRatios.window >= aspectRatios.stage){
            scale = windowDimensions.height / stageDimensions.height;
        }
        else{
            scale = windowDimensions.width / stageDimensions.width;
        }
        
        // scale = windowDimensions.height / stageDimensions.height;

        const transformedStageDimensions = {
            height: stageDimensions.height * scale,
            width: stageDimensions.width * scale,
        }

        return {
            $stage: $stage,
            vertical: aspectRatios.window < 1,
            scale: scale,
            windowDimensions: windowDimensions,
            stageDimensions: stageDimensions,
            aspectRatios: aspectRatios,
            transformedStageDimensions: transformedStageDimensions,
            left: Math.floor((windowDimensions.width - transformedStageDimensions.width) / 2),
            top: Math.floor((windowDimensions.height - transformedStageDimensions.height) / 2),
        }
    },
    resizeStage: function(args){
        var data = this.getStageTransformedData();

        if(data.vertical){
            // data.$stage.addClass('vertical');
            $('.rotate-device').removeClass('hidden');
        }
        else{
            $('.rotate-device').addClass('hidden');
            // data.$stage.removeClass('vertical');
        }

        var repositionSlide = false;

        if(data.vertical !== this.currentProps.vertical){
            data = this.getStageTransformedData();
            data.newOrientation = true;
            repositionSlide = true;
        }

        this.currentProps = data;
        data.$stage.css({
            'display': 'block',
			'transform-origin': '0% 0%',
			'transform': 'scale(' + data.scale + ')',
			'left' : data.left + 'px',
			'top' : data.top + 'px',
			'display' : 'block'
        }).trigger('stageResize');

        if(this.resizeStage.prototype.ready !== true){
            if(typeof this.ready === 'function'){
                this.ready();
            }
            this.resizeStage.prototype.ready = true;
        }
        
        return data;
    },
    addWindowResizeEvents: function(){
        $(window).on('resize', function(){
            setTimeout(function(){
                module.exports.resizeStage();
                
            }, 50);
        });
    }
};